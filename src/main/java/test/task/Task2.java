package test.task;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bogdan Sliptsov
 *
 * This class is used to get all integer dividers of number.
 */
public class Task2 {

    /**
     * This method is used to find all integer dividers of number.
     *
     * @param number Integer number, dividers of which you want find.
     * @return List<Integer> List of integer dividers.
     */
    private static List<Integer> getDividers(int number) {
        List<Integer> dividers = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) dividers.add(i);
        }
        return dividers;
    }

    public static void main(String[] args) {
        System.out.println(getDividers(15));
    }
}
