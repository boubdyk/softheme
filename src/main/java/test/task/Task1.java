package test.task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * @author Bogdan Sliptsov
 *
 * This class is used to find the longest sequence of "0"
 */

public class Task1 {

    static final String INPUT_FILE_NAME = "D:\\INPUT.txt";
    static final String OUTPUT_FILE_NAME = "D:\\OUTPUT.txt";
    private String input;
    private String output;
    private ArrayList lngthArr;

    /**
     * This method is used to read data from input file.
     *
     * @param fileName Name of input file in Windows file system.
     * @return Input data.
     */

    private static StringBuffer readFromFile(String fileName) {
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        try{
            String line = null;
            br = new BufferedReader(new FileReader(fileName));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) { /*NOP*/ }
        }
        return sb;
    }

    /**
     * This method is used to write data to output file.
     *
     * @param fileName Output file name in Windows file system.
     * @param outputLength Length of the longest sequence of "0"
     * @return true If data was written to output file.
     *         false If data wasn't written to output file.
     */
    private static boolean writeToFile(String fileName, Integer outputLength) {
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(outputLength.toString());
            bw.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * This method is used to finde the longest sequence of "0".
     *
     * @param input Data which has been read from input file.
     * @return int Length of the longest sequence of "0".
     */
    private static int maxLength(StringBuffer input) {
        if (StringUtils.isEmpty(input.toString())) return 0;
        String in = input.toString();
        String[] sequences = in.split("1");
        List<Integer> sizes = new ArrayList<>();
        for (String str: sequences) {
            sizes.add(str.length());
        }
        Collections.sort(sizes);
        return sizes.get(sizes.size() - 1);
    }

    public static void main(String[] args) {
        System.out.println(writeToFile(OUTPUT_FILE_NAME, maxLength(readFromFile(INPUT_FILE_NAME))));
    }

}